from functools import wraps

def check_argsTypes(**decls):
    """Декоратор который проверяет тип аргументов.

    Использование:

    @check_argsTypes(name=str, data=(str,Type(None)) )
    def foo(name, data)
    """

    def decorator(func):
        code = func.__code__
        fname = func.__name__
        names = code.co_varnames[:code.co_argcount]
        @wraps(func)
        def decorated(*args,**kwargs):
            for argname, argtype in decls.items():
                try:
                    argval = args[names.index(argname)]
                except ValueError:
                    argval = kwargs.get(argname)
                if not isinstance(argval, argtype):
                    raise TypeError("%s(...): arg '%s': type is %s, must be %s" % (fname, argname, type(argval), str(argtype)))
            return func(*args,**kwargs)
        return decorated
    return decorator



import json

@check_argsTypes(data = (dict,list,type(None),str))
def from_data_to_string(data):
    """
    Преобразует входящий объект в строку
    """

    if   isinstance(data,type(None)):
        return ''
    elif isinstance(data, (dict, list)):
        return json.dumps(data)
    elif isinstance(data,str):
        return data

    raise SystemError('Unexpected behavior, none of the handlers did not worked')

@check_argsTypes(data = (dict,list,type(None),str))
def from_data_to_dict(data):
    """
    Преобразует входящий объект в словарь
    """ 

    if   isinstance(data,str):
        data = json.loads(data)
        if isinstance(data,dict):
            return data
        else:
            return {}
    elif isinstance(data, (type(None),list) ):
        return {}       
    elif isinstance(data,dict):
        return data

    raise SystemError('Unexpected behavior, none of the handlers did not worked')