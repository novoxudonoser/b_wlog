import json
import pika
import sys
import time
import traceback


from qps.application import app


DELAY_MAINLOOP = 0.1

from app.main import on_receive

class Worker:

    def start(self):
        app.rabbit.channel.queue_declare(queue='log', durable=True)
        app.rabbit.channel.queue_declare(queue='bad_logs', durable=True)

        app.rabbit.channel.exchange_declare(
            exchange = 'log',
            type = 'topic',
            durable = True)

        app.rabbit.channel.exchange_declare(
            exchange = 'bad_logs',
            type = 'topic',
            durable = True)

        app.rabbit.channel.queue_bind(
            exchange='log',
            queue='log',
            routing_key='log')

        app.rabbit.channel.queue_bind(
            exchange='bad_logs',
            queue='bad_logs',
            routing_key='bad_logs')   

        app.rabbit.channel.basic_qos(prefetch_count=1)
        app.rabbit.channel.basic_consume(on_receive, queue='log')

    def mainLoop(self):
        app.rabbit.channel.start_consuming()