import random
import string
import json
from datetime import datetime
import pprint
import logging
import pika

from dateutil import parser as dateutil_parser
from dateutil import tz as dateutil_tz

import xmltodict
import traceback

from app.models import Postomat,Agregator,Channel,Log_api,Log_api_To_Command,Log_api_To_Postomat,PostomatLogRecord
from app.models import Log_api_To_Parcel, Component, Activity, Command, Parcel, Log_activity, Log_api_To_Postomat

from qps.application import app

from app.utils import from_data_to_string, from_data_to_dict

directions_ids={
    "IN":1,
    "OUT":2
}





def standart_api_processor(resived_data):

    timestamp     = datetime.strptime(resived_data['timestamp'], "%Y-%m-%d %H:%M:%S.%f")
    category      = resived_data['category']
    module        = resived_data['module'] 
    corellationId = resived_data['corellationId'][:50]

    version       = str(resived_data['version'])[:10]
    data          = resived_data['data']
    direction     = directions_ids[data['direction']]

    headers       = data['headers']
    body          = data['body']

    channel_name = data['channel']


    if module:
        component = app.db.session.query(Component).filter_by(name=module).first()
        if not component:
                component=Component(name=module)
                app.db.session.add(component)
    else:
        component = None

    if channel_name:
        channel = app.db.session.query(Channel).filter_by(channelName=channel_name).first()
        if not channel:
                channel=Channel(channelName=channel_name)
                app.db.session.add(channel)
    else:
        channel = None

    agregator_name = module # ispravit kogda reshim otkuda brat dannie
    if agregator_name:
        agregator = app.db.session.query(Agregator).filter_by(agregatorId=agregator_name).first()
        if not agregator:
            agregator=Agregator(agregatorId=agregator_name)
            app.db.session.add(agregator)
    else:
        agregator = None 

    new_api_record = Log_api(
        component     = component,
        channel       = channel,
        agregator     = agregator,
        timestamp     = datetime.now(),
        timestamp_api = timestamp,
        headers       = from_data_to_string(headers),
        body          = from_data_to_string(body),
        corellationId = corellationId,
        direction     = direction,
        category      = category,
        version       = version,
        )

    app.db.session.add(new_api_record)

    return new_api_record

def standart_api_handler(ch, pika_method, props, resived_data):
    logging.info('Process API standart')
    standart_api_processor(resived_data)
    app.db.session.commit()



def handle_api_INTERFACE_PICKPOINT(ch, pika_method, props, resived_data):
    logging.info('Process API PICKPOINT')

    new_api_record = standart_api_processor(resived_data)
    new_api_record.counterparty = new_api_record.agregator.agregatorId

    data          = resived_data['data']
    body          = data['body']
    body_data     = xmltodict.parse(body)

    InvoiceList=[]

    if "InvoiceList" in body_data:
        Invoice=body_data["InvoiceList"]["Invoice"]
        if not isinstance(Invoice,list):
            InvoiceList=[Invoice]
        else:
            InvoiceList=Invoice
    if "CancelList" in body_data:
        Invoice=body_data["CancelList"]["Invoice"]
        if not isinstance(Invoice,list):
            InvoiceList=[Invoice]
        else:
            InvoiceList=Invoice
    if "Invoice" in body_data:
        InvoiceList=[body_data["Invoice"]]
    if "OpenCellDoor" in body_data:
        InvoiceList=[body_data["OpenCellDoor"]]
    if "Filter" in body_data:
        InvoiceList=[body_data["Filter"]]

    method = data['method']
    component = new_api_record.component
    if method and component:
        command  = app.db.session.query(Command).filter_by(name=method,component=component).first()
        if not command:
            command=Command(name=method,component=component)
            app.db.session.add(command)
    else:
        command = Non

    association=Log_api_To_Command()
    association.command=command
    association.log_api=new_api_record
    app.db.session.add(association)

    for Invoice in InvoiceList:
        postamatId  = Invoice.get("@PostamatNumber")
        if postamatId:
            postomat = app.db.session.query(Postomat).filter_by(postamatId=postamatId).first()
            if not postomat:
                postomat=Postomat(postamatId=postamatId)
                app.db.session.add(postomat)

            old_association = app.db.session.query(Log_api_To_Postomat).filter_by(
                            log_api=new_api_record,
                            postomat=postomat).first()

            if not old_association:
                association=Log_api_To_Postomat(
                    log_api=new_api_record,
                    postomat=postomat)
                app.db.session.add(association)

    app.db.session.commit()



def handle_api_INTERFACE_POSTAMAT(ch, pika_method, props, resived_data):

    logging.info('Process API POSTAMAT')

    module        = resived_data['module'] 
    data          = resived_data['data']
    body          = data['body']
    corellationId = resived_data['corellationId'][:50]
    
    body_data     = from_data_to_dict(body)

    postamatId  = body_data.get('postamatId')

    if not postamatId: # take postomat id from past logs
        other_log = app.db.session.query(Log_api).filter_by(corellationId = corellationId).first()
        postamatId = other_log.counterparty if other_log else None

    if postamatId:
        postomat = app.db.session.query(Postomat).filter_by(postamatId=postamatId).first()
        if not postomat:
            postomat=Postomat(postamatId=postamatId)
            app.db.session.add(postomat)
    else:
        postomat = None


    new_api_record = standart_api_processor(resived_data)
    new_api_record.counterparty = postamatId

    if postomat:
        association=Log_api_To_Postomat()
        association.log_api=new_api_record
        association.postomat=postomat
        app.db.session.add(association)

    for request in  body_data.get('request',[]):
        method = request["method"]
        request=request if isinstance(request,dict) else {}
        params = request.get('params',{})
        params=params if isinstance(params,dict) else {}

        for parsel_obj in params.get('parcels',[]):
            parsel_obj=parsel_obj if isinstance(parsel_obj,dict) else {}
            parcelId = str(parsel_obj.get('parcelId'))
            if parcelId:
                parsel = app.db.session.query(Parcel).filter_by(ParcelId=parcelId).first()
                if not parsel:
                    parsel = Parcel(ParcelId=parcelId)
                    app.db.session.add(parsel)

                old_association = app.db.session.query(Log_api_To_Parcel).filter_by(
                                parsel=parsel,
                                log_api=new_api_record).first()

                if not old_association:
                    association=Log_api_To_Parcel(
                        parsel=parsel,
                        log_api=new_api_record)
                    app.db.session.add(association)

            command_name = method
            if command_name:
                command = app.db.session.query(Command).filter_by(name=command_name,component=new_api_record.component).first()
                if not command:
                    command=Command(name=command_name, component=new_api_record.component)
                    app.db.session.add(command)
            else:
                command = None

            old_association = app.db.session.query(Log_api_To_Command).filter_by(
                            command=command,
                            log_api=new_api_record).first()

            if not old_association:
                association=Log_api_To_Command(
                    command=command,
                    log_api=new_api_record)
                app.db.session.add(association)


    for response in body_data.get('response',[]):
        if response.get('reqid') == "8888":
            for logrecord in response["params"]["logs"]:
                module    = logrecord["module"]
                data      = logrecord["data"]
                logrecord["timestamp"]=logrecord["timestamp"]
                timestamp = dateutil_parser.parse(logrecord["timestamp"])
                parcelId  = str(logrecord["parcelId"])

                if parcelId:
                    parsel = app.db.session.query(Parcel).filter_by(ParcelId=parcelId).first()
                    if not parsel:
                        parsel = Parcel(ParcelId=parcelId)
                        app.db.session.add(parsel)
                else:
                    parsel = None

                newrecord = PostomatLogRecord(
                    module      = module,
                    timestamp   = timestamp,
                    data        = data,
                    postamat    = postomat,
                    parcel      = parsel,
                )
                app.db.session.add(newrecord)

    app.db.session.commit()


def handle_activity(ch, method, props, resived_data):

    logging.info('Process Activity')

    timestamp     = datetime.strptime(resived_data['timestamp'], "%Y-%m-%d %H:%M:%S.%f")
    category      = resived_data['category']
    module        = resived_data['module']
    corellationId = resived_data['corellationId'][:50]
    version       = str(resived_data['version'])[:10]
    data          = resived_data['data']

    method   = data['method']

    parcelId = str(data.get('parcelId'))
    context  = data['context']
    body     = data['body']
    result   = data['result']
    action   = data['action']


    if parcelId:
        parsel   = app.db.session.query(Parcel).filter_by(ParcelId=parcelId).first()
        if not parsel:
            parsel = Parcel(ParcelId=parcelId)
            app.db.session.add(parsel)
    else:
        parsel=None

    component=app.db.session.query(Component).filter_by(name=module).first()
    if not component:
        component=Component(name=module)
        app.db.session.add(component)

    activity = app.db.session.query(Activity).filter_by(name=action).first()
    if not activity:
        activity=Activity(name=action,component=component)
        app.db.session.add(activity)

    if method and component:
        command  = app.db.session.query(Command).filter_by(name=method,component=component).first()
        if not command:
            command=Command(name=method,component=component)
            app.db.session.add(command)
    else:
        command = None


    new_activity_record = Log_activity(
        activity           = activity,
        component          = component,
        parcel             = parsel,
        command            = command,
        timestamp_activity = timestamp,
        timestamp          = datetime.now(),
        context            = from_data_to_string(context),
        body               = from_data_to_string(body),
        corellationId      = corellationId,
        result             = from_data_to_string(result),
        category           = category,
        version            = version,
        )

    app.db.session.add(new_activity_record)
    app.db.session.commit()


api_handlers={
    "INTERFACE_POSTAMAT": handle_api_INTERFACE_POSTAMAT,
    "INTERFACE_PICKPOINT":handle_api_INTERFACE_PICKPOINT,
}



def on_receive(ch, method, props, body):
    try:
        resived_data=json.loads(body.decode('utf-8'))
        logging.debug(pprint.pformat(resived_data, indent=4))        
        if resived_data["type"] == "api":

           handler = api_handlers.get(resived_data["module"],standart_api_handler)
           handler(ch, method, props, resived_data)

        if resived_data["type"] == "activity":

           handle_activity(ch, method, props, resived_data)

        ch.basic_ack(delivery_tag = method.delivery_tag)

    except:
        try:
            logging.error(traceback.format_exc())
            app.rabbit.channel.basic_publish(
               exchange = 'bad_logs',
               routing_key = 'bad_logs',
               body = body,
               properties = pika.BasicProperties(delivery_mode=2)
               )
            ch.basic_ack(delivery_tag = method.delivery_tag)
        finally:
            app.db.session.rollback()

        

