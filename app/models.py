import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, BigInteger, DateTime, Boolean, Text

from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


# todo  : добавить nullable на ForeignKey , там где это будет необходимо

class PostomatLogRecord(Base):
    __tablename__ = 'PostomatLogRecords'

    id          = Column(Integer, primary_key=True)
    module      = Column(String(50))
    timestamp   = Column(DateTime)
    data        = Column(Text())

    postamat_id = Column(Integer, ForeignKey('Postomats.id'))
    postamat    = relationship("Postomat", back_populates="internallogs")

    parcel_id   = Column(Integer, ForeignKey('Parcels.id'))
    parcel      = relationship("Parcel", back_populates="internallogs")


class Parcel(Base):
    __tablename__ = 'Parcels'

    id             = Column(Integer, primary_key=True)
    ParcelId       = Column(String(50), unique=True)
    activitys_logs = relationship("Log_activity")
    log_apis       = relationship("Log_api_To_Parcel", back_populates="parsel")
    internallogs = relationship("PostomatLogRecord", back_populates="parcel")


class Postomat(Base):
    __tablename__ = 'Postomats'

    id           = Column(Integer, primary_key=True)
    postamatId   = Column(String(50), nullable=False, unique=True)
    log_apis     = relationship("Log_api_To_Postomat", back_populates="postomat")
    internallogs = relationship("PostomatLogRecord", back_populates="postamat")


class Agregator(Base):
    __tablename__ = 'Agregators'

    id          = Column(Integer, primary_key=True)
    agregatorId = Column(String(50), nullable=False, unique=True)
    log_apis    = relationship("Log_api", back_populates="agregator")


class Channel(Base):
    __tablename__ = 'Channel'

    id          = Column(Integer, primary_key=True)
    channelName = Column(String(50), nullable=False, unique=True)
    log_apis    = relationship("Log_api", back_populates="channel")


class Log_api(Base):
    __tablename__ = 'log_api'

    id            = Column(Integer, primary_key=True)
    channel_id    = Column(Integer, ForeignKey('Channel.id'))
    channel       = relationship("Channel",   back_populates="log_apis")
    agregator_id  = Column(Integer, ForeignKey('Agregators.id'))
    agregator     = relationship("Agregator", back_populates="log_apis")
    postomats     = relationship("Log_api_To_Postomat", back_populates="log_api")
    commands      = relationship("Log_api_To_Command", back_populates="log_api")
    parcels       = relationship("Log_api_To_Parcel", back_populates="log_api")
    counterparty  = Column(String(50))
    timestamp     = Column(DateTime, nullable=False)
    timestamp_api = Column(DateTime) # время в пакете
    headers       = Column(Text())
    body          = Column(Text())
    corellationId = Column(String(50))
    direction     = Column(Integer)
    category      = Column(String(50))
    version       = Column(String(10))

    component_id   = Column(Integer, ForeignKey('components.id'))
    component      = relationship("Component", back_populates="log_apis")    


class Log_api_To_Postomat(Base):
    __tablename__ = 'la_to_Postomats'

    log_api_id  = Column(Integer, ForeignKey('log_api.id'), primary_key=True)
    postomat_id = Column(Integer, ForeignKey('Postomats.id'), primary_key=True)
    postomat    = relationship("Postomat", back_populates="log_apis")
    log_api     = relationship("Log_api", back_populates="postomats")


class Log_api_To_Command(Base):
    __tablename__ = 'la_to_command'

    log_api_id = Column(Integer, ForeignKey('log_api.id'), primary_key=True)
    command_id = Column(Integer, ForeignKey('Commands.id'), primary_key=True)
    command    = relationship("Command", back_populates="log_apis")
    log_api    = relationship("Log_api", back_populates="commands")


class Log_api_To_Parcel(Base):
    __tablename__ = 'la_to_parsel'

    log_api_id = Column(Integer, ForeignKey('log_api.id'), primary_key=True)
    parsel_id  = Column(Integer, ForeignKey('Parcels.id'), primary_key=True)
    parsel     = relationship("Parcel", back_populates="log_apis")
    log_api    = relationship("Log_api", back_populates="parcels")





class Component(Base):
    __tablename__ = 'components'

    id          = Column(Integer, primary_key=True)
    name        = Column(String(50), nullable=False, unique=True)
    description = Column(Text())

    activitys   = relationship("Activity", back_populates="component")
    commands    = relationship("Command", back_populates="component")

    log_apis      = relationship("Log_api",      back_populates="component")
    log_activitys = relationship("Log_activity", back_populates="component")




class Activity(Base):
    __tablename__ = 'activities'

    id             = Column(Integer, primary_key=True)
    name           = Column(String(50), nullable=False)

    component_id   = Column(Integer, ForeignKey('components.id'))
    component      = relationship("Component", back_populates="activitys")

    activitys_logs = relationship("Log_activity", back_populates="activity")



class Command(Base):
    __tablename__ = 'Commands'

    id             = Column(Integer, primary_key=True)
    name           = Column(String(50))

    component_id   = Column(Integer, ForeignKey('components.id'))
    component      = relationship("Component", back_populates="commands")

    activitys_logs = relationship("Log_activity", back_populates="command")

    log_apis       = relationship("Log_api_To_Command", back_populates="command")



class Log_activity(Base):
    __tablename__ = 'log_activity'

    id                 = Column(Integer, primary_key=True)
    activity_id        = Column(Integer, ForeignKey('activities.id'))
    activity           = relationship("Activity", back_populates="activitys_logs")
    parcel_id          = Column(Integer, ForeignKey('Parcels.id'))
    parcel             = relationship("Parcel", back_populates="activitys_logs")
    command_id         = Column(Integer, ForeignKey('Commands.id'))
    command            = relationship("Command", back_populates="activitys_logs")
    timestamp          = Column(DateTime, nullable=False)
    timestamp_activity = Column(DateTime, nullable=False)
    context            = Column(Text())
    body               = Column(Text())
    corellationId      = Column(String(50))
    result             = Column(Text())
    category           = Column(String(50))
    version            = Column(String(10))

    component_id   = Column(Integer, ForeignKey('components.id'))
    component      = relationship("Component", back_populates="log_activitys")    

