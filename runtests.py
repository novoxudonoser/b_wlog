#!/usr/bin/env python

from qps.application import app
from qpsdev.testing import runtests


tests = [
    'testing.nonetest',
]

r = runtests.all(tests)
app.sys_exit(success=r)


