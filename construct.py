import qps.bizlog
import qps.util
import qps.db

from app import wserv

from app import models
import qps.rmq

def construct(app):
    app.module_type = 'WORKER_BIZLOG'

    db = app.component('db', qps.db.DB)
    db.bind_metadata(models.Base.metadata)

    rmq = app.component('rabbit', qps.rmq.RabbitMQ)

    log = app.component('log', qps.bizlog.BizLog)

    app.component('server', wserv.Worker, mainLoop=True)



