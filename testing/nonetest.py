import unittest

import datetime
import jinja2
import pika
import requests
import sys
import time
import tornado.ioloop
import tornado.wsgi
import tornado.httpserver
import traceback
import xmltodict
import yaml

from qps.application import app
from qpsdev.testing.interaction import Interact, Process
from qpsdev.util.tornado import tornado_ioloop_reset, tornado_set_check_stop_callback
from qpsdev.testing import rmq
from qpsdev.testing.config import load_testing_config


from decimal import getcontext,Decimal

class TC(unittest.TestCase):

    def setUp(self):
        self.testing_config = load_testing_config()

        app.reset()
        self.rabbit_connection = pika.BlockingConnection(pika.URLParameters(self.testing_config['rabbit']['url']))
        self.rabbit_channel = self.rabbit_connection.channel()

        # rmq.read_queue(self.rabbit_channel, queue='payments_resend')
        # rmq.read_queue(self.rabbit_channel, queue='payments_repay')
        # rmq.read_queue(self.rabbit_channel, queue='balances')
        # rmq.read_queue(self.rabbit_channel, queue='payments')
        # rmq.read_queue(self.rabbit_channel, queue='log')

        # db = init_db('db',self.testing_config)
        # make_test_db_ok(db)
        # db.stop()


    def tearDown(self):
        pass
        # rmq.read_queue(self.rabbit_channel, queue='payments_resend')
        # rmq.read_queue(self.rabbit_channel, queue='payments_repay')
        # rmq.read_queue(self.rabbit_channel, queue='balances')
        # rmq.read_queue(self.rabbit_channel, queue='payments')
        # rmq.read_queue(self.rabbit_channel, queue='log')

        # db = init_db('db', self.testing_config)
        # clean_db(db)
        # db.stop()


    def test1(self):
        def worker(interact):
            try:
                app.config = self.testing_config
                app.construct()
                app.initialize_components()
                app.start()
                interact.ready()
                rmq.main_loop(app.rabbit.connection, interact, delay=1)
            except:
                tr=traceback.format_exc()
                print(tr, file=sys.stderr)
                raise


        def action(interact):
            time.sleep(0.5)
            rabbit_connection = pika.BlockingConnection(pika.URLParameters(self.testing_config['rabbit']['url']))
            rabbit_channel = self.rabbit_connection.channel()

            #work

            time.sleep(0.5)

            while True:
                r = rabbit_channel.queue_declare(queue='payments', durable=True)
                if r.method.message_count==0:
                    break
                time.sleep(0.1)

            time.sleep(3)

        interact = Interact(
            processes = dict(
                worker = Process(worker, main=True),
                action = Process(action, action=True),
            )
        )

        interact.run()

        self.assertTrue(interact.results.action.success)
        self.assertTrue(interact.results.worker.success)


def suite():

    return unittest.TestSuite([
        TC('test1'),
    ])