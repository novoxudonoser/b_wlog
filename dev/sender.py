import pika
import json

connection = pika.BlockingConnection(pika.URLParameters('ampq://127.0.0.1'))
channel = connection.channel()


data={
    "postamatId":"0005-000",
    "request":[
        {
            "method":"newParcelEvent",
            "id":"143955754600000002",
            "params":{
                "parcels":[
                    {
                        "parcelId":855,
                        "event":5,
                        "time":"2016-03-04T13:20:03+02:00",
                        "comments":
                            {
                                "sum":"15",
                                "payId":"1439572820"
                            }
                    }
                ]
            }
        }
    ]
}


mesage1={
    "type":"api",
    "timestamp": "2016-03-09 19:02:50.240967" ,
    "module": "INTERFACE_POSTAMAT",
    "category": "INFO",
    "corellationId":"ASDASD",
    "version": "1.0",

    "data": {
        "headers":"hq hwe",
        "direction":"IN",
        "body":data,
        "channel":"TERMINALL",
    }
}


mesage2={  
  'type':'activity',
  'timestamp':'2016-04-13 18:43:45.867263',
  'category':'INFO',
  'data':{  
     'result':'234',
     'parcelId':'123',
     'action':'Message_processing',
     'context':{  
        'data':{  
           'request':[  
              {  
                 'params':{  
                    'parcels':[  
                       {  
                          'parcelId':213,
                          'event':5,
                          'time':'2015-03-20T10:57:03+02:00',
                          'comments':{  
                             'TerminalTime':'2015-03-20T10:57:03+02:00',
                             'Sum':'1100',
                             'QiwiTerminalId':'1233214',
                             'QiwiTrmTxnNumber':'1234879739859'
                          }
                       },
                       {  
                          'parcelId':345,
                          'event':5,
                          'time':'2015-03-20T10:57:03+02:00',
                          'comments':{  
                             'TerminalTime':'2015-03-20T10:57:03+02:00',
                             'Sum':'1900',
                             'QiwiTerminalId':'1234',
                             'QiwiTrmTxnNumber':'1234'
                          }
                       }
                    ],
                    'encloses':[  
                       {  
                          'event':4,
                          'time':'2015-03-20T10:27:03+02:00',
                          'comments':{  
                             'authorizeId':'8c55d2f5aa9326d39aa2f7864c1ba6a1',
                             'cellId':'C1N4'
                          },
                          'barCodeMD5':'ccde9f5a321d3f5d689efe4e8f5339da'
                       },
                       {  
                          'event':10,
                          'time':'2015-03-20T10:57:03+02:00',
                          'comments':{  
                             'changeTrmTxnId':'31357887285',
                             'payTrmTxnId':'31357887282',
                             'clientPhone':'+7(926)1234153'
                          },
                          'barCodeMD5':'d37249e329616a77cc7c2fcc8d795b93'
                       }
                    ]
                 },
                 'id':'130705432750000000',
                 'method':'newParcelEvent'
              }
           ],
           'postamatId':'0005-000'
        },
        'correlationId':'123'
     },
     'method':'asdad',
     'body':None
  },
  'corellationId':'123',
  'module':'WORKER_PAYMENTS_1',
  'version':0.1
}


mesage=json.dumps(mesage2).encode()


channel.exchange_declare(exchange = 'events',type = 'topic',durable = True)
channel.basic_publish(
    exchange = 'log',
    routing_key = 'log',
    body = mesage,
    properties = pika.BasicProperties(delivery_mode=2)
)
