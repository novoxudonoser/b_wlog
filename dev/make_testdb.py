#!/usr/bin/env python
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

from app.models import Postomat,Agregator,Channel,Log_api,Log_api_To_Command
from app.models import Log_api_To_Parcel, Component, Activity, Command, Parcel, Log_activity

from qps.application import app

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import contextlib
from sqlalchemy import MetaData
from app.models import Base

from qps.db import DB
from sqlalchemy.exc import OperationalError
from qpsdev.testing.config import load_testing_config

def init_db(name,config):
    db=DB()
    db.config = config
    db.base = Base
    db.construct_configure(name,config,None)
    db.start()
    return db


def clean_db(db):
    meta = Base.metadata
    with contextlib.closing(db.engine.connect()) as con:
        trans = con.begin()
        for table in reversed(db.metadata.sorted_tables):
            try:
                con.execute(table.delete()) 
            except OperationalError:
                pass            
        trans.commit()  


def make_clean_db(db):
    clean_db(db)
    db.base.metadata.create_all()


def make_test_db(db):
    make_clean_db(db)

    # component1 = Component(name="INTERFACE_PICKPOINT")
    # component2 = Component(name="INTERFACE_TERMINAL")
    # db.session.add(component1)
    # db.session.add(component2)
    # component3 = Component(name="WORKER_PAYMENT")
    # component4 = Component(name="INTERFACE_COMMANDER")
    # db.session.add(component3)
    # db.session.add(component4)

    # db.session.commit()



if __name__ == "__main__":
    testing_config = load_testing_config()
    db = init_db('db', testing_config)
    make_test_db(db)
    db.stop()