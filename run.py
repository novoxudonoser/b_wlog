#!/usr/bin/env python

from qps.application import app
from construct import construct

app.load_config()
construct(app)
app.initialize_components()
app.start()
app.mainLoop()
